-- phpMyAdmin SQL Dump
-- version 2.8.0.1
-- http://www.phpmyadmin.net
-- 
-- Servidor: custsql-ipg07.eigbox.net
-- Tiempo de generación: 23-01-2014 a las 18:49:16
-- Versión del servidor: 5.5.32
-- Versión de PHP: 4.4.9
-- 
-- Base de datos: `smartphuy_bd1`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `categoria`
-- 

CREATE TABLE `categoria` (
  `codigo_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(200) NOT NULL,
  `categoria_padre` int(11) DEFAULT NULL,
  PRIMARY KEY (`codigo_categoria`),
  KEY `categoria_padre` (`categoria_padre`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `factura`
-- 

CREATE TABLE `factura` (
  `codigo_factura` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `total` float NOT NULL,
  `estado` varchar(100) NOT NULL,
  `direccion_envio` varchar(500) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`codigo_factura`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `lista_compra`
-- 

CREATE TABLE `lista_compra` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_factura` int(11) NOT NULL,
  `codigo_producto` varchar(200) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigo_factura` (`codigo_factura`),
  KEY `codigo_producto` (`codigo_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `producto`
-- 

CREATE TABLE `producto` (
  `codigo_producto` varchar(200) NOT NULL,
  `codigo_sistema_interno` text NOT NULL,
  `nombre` varchar(22) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` float NOT NULL,
  `codigo_categoria` int(11) NOT NULL,
  `cantidad_limite` int(11) NOT NULL,
  `cantidad_inventario` int(11) NOT NULL,
  `foto` text NOT NULL,
  PRIMARY KEY (`codigo_producto`),
  KEY `codigo_categoria` (`codigo_categoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Filtros para las tablas descargadas (dump)
-- 

-- 
-- Filtros para la tabla `categoria`
-- 
ALTER TABLE `categoria`
  ADD CONSTRAINT `categoria_ibfk_1` FOREIGN KEY (`categoria_padre`) REFERENCES `categoria` (`codigo_categoria`) ON UPDATE CASCADE;

-- 
-- Filtros para la tabla `producto`
-- 
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`codigo_categoria`) REFERENCES `categoria` (`codigo_categoria`) ON UPDATE CASCADE;
