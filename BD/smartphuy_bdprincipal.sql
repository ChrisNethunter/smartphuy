-- phpMyAdmin SQL Dump
-- version 2.8.0.1
-- http://www.phpmyadmin.net
-- 
-- Servidor: custsql-ipg07.eigbox.net
-- Tiempo de generación: 23-01-2014 a las 18:48:41
-- Versión del servidor: 5.5.32
-- Versión de PHP: 4.4.9
-- 
-- Base de datos: `smartphuy_bdprincipal`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `mercados`
-- 

CREATE TABLE `mercados` (
  `codigo_mercado` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `frase` varchar(200) NOT NULL,
  `imagen` text NOT NULL,
  PRIMARY KEY (`codigo_mercado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `user_act`
-- 

CREATE TABLE `user_act` (
  `codigo_usuario` int(15) NOT NULL,
  `mercado` int(11) DEFAULT NULL,
  `hist_mercado` int(11) DEFAULT NULL,
  `factura` int(11) DEFAULT NULL,
  PRIMARY KEY (`codigo_usuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `usuarios`
-- 

CREATE TABLE `usuarios` (
  `codigo_usuario` int(11) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `apellido` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `tel_fijo` int(12) DEFAULT NULL,
  `celular` int(11) NOT NULL,
  `direccion` text NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`codigo_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
