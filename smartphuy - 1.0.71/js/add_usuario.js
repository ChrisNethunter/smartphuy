/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
$('#Page_addUsuario').live('pageshow', function(event) 
{
	iniciar();
});
function iniciar()
{
	$("#form_registro_User").submit(addUsuario);
}
function addUsuario(evento)
{ 
	evento.preventDefault();
	
	var cedula = $('#cedula').val();
	var nombre = $('#nombre').val();
	var apellido = $('#apellido').val();
	var correo = $('#correo').val();
	var direccion = $('#direccion').val();
	var telefono = $('#telefono').val();
	var celular = $('#celular').val();
	var contrasena = $('#contrasena').val();
	var new_C = $('#contrasena').val();
	var rep_C = $('#rep_Contrasena').val();
	//navigator.notification.alert('cedula:'+cedula+ 'nombre:'+nombre+'apellido:'+apellido+'correo:'+correo+'direccion:'+direccion+'celular:'+celular+'contraseña:'+contrasena);
	
	if(telefono == 0)
	{
		telefono=0;
	}
	
	if (cedula.length > 0 && nombre.length > 0 && apellido.length > 0 &&  correo.length > 0 && direccion.length > 0 && celular.length > 0 && contrasena.length > 0)
	{ 
		if( String(rep_C) == String(new_C))
		{
			var datos_formulario = $(this).serialize();
			$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+cedula).done(function(data)
			{	
				var datos = data.items;
				$.each(datos, function(index, dato) 
				{  
					if(dato.codigo_usuario == cedula)
					{
						navigator.notification.alert("El usuario ya se encuantra registrado", null, 'Information');
						window.open('index.html');
					}
				});
				if(data.items == 0)
				{
					$.ajax({
							data: datos_formulario,
							type: 'post',
							dataType: 'json',
							url: 'http://new-master.com/smartphuy_addusuario.php?telefono='+telefono,
							success: function(jsonObj)
							{
								navigator.notification.alert("Se ha registrado correctamente", null, 'Information');
								window.open('index.html');
							},
							error: function() 
							{
								navigator.notification.alert("Su informacion no se ha podido añadir", null, 'Information');
							}
						});
				}
			});
		}
		else
		{
			navigator.notification.alert("La nueva Contraseña NO Coincide", null, 'Information');
		}				
	}
	
	else
	{
		navigator.notification.alert("Su informacion esta INCOMPLETA", null, 'Information');
	}
	
}

function validateMail(idMail)
{
	//Creamos un objeto 
	object=document.getElementById(idMail);
	valueForm=object.value;

	// Patron para el correo
	var patron=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	if(valueForm.search(patron)==0)
	{
		//Mail correcto
		object.style.color="#000";
		return;
	}
	//Mail incorrecto
	object.style.color="#f00";
}

function validarcontrasena()
{
	if($('#contrasena').val() == 0)
	{
		$('#contrasena').attr('style','box-shadow: 0 0 5px 5px #FF0000');
	}
	else
	{
		$('#contrasena').removeAttr('style');
	}
	
	if( String($('#rep_Contrasena').val()) != String($('#contrasena').val()))
	{
		$('#rep_Contrasena').attr('style','box-shadow: 0 0 5px 5px #FF0000');
	}
	else
	{
		$('#rep_Contrasena').removeAttr('style');
	}
}