/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
var datos_compras;
$('#Page_ConfirmBuy').live('pageshow', function(event) 
{	
	inicializarConfirmBuy();
	$('#form_ConfirmBuy').submit(confirmar_compra);	
});
function inicializarConfirmBuy()
{
$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"] ).done(function(data) 
	{
		$('#Lista_compra_actual li').remove();
		var datosA = data.items;		
		$.each(datosA, function(index, datoA) 
		{
			$.getJSON(serviceURL + 'smartphuy_datosMercados.php?codigo_mercado='+datoA.mercado).done(function(data) 
			{
				datos_compras = data.items;	
				$.each(datos_compras, function(index, datos_compra) 
				{
					$('#img_mercado').attr('src',serviceURL+'smartphuy_img/'+datos_compra.imagen);
					$('#content_mercado').html('<h1>'+datos_compra.nombre+'</h1>'+
								'<p>'+datos_compra.frase+'</p>');
				});			
			});	
			$.getJSON(serviceURL+'smartphuy_datosFactura.php?mercado='+datoA.mercado+'&codigo='+datoA.factura).done(function(data) 
			{
				datos_compras = data.items;	
				$.each(datos_compras, function(index, datos_compra) 
				{
					$('#content_Factura').html('<td colspan="2">Total: '+datos_compra.total+'</td>');
				});			
			});
			$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+datoA.codigo_usuario).done(function(data) 
			{
				datos_compras = data.items;	
				$.each(datos_compras, function(index, datos_compra) 
				{
					$('#content_Usuario').html('<td colspan="2">'+
								'<p>'+datos_compra.nombre+' '+datos_compra.apellido+'</p>'+                     
								'<input type="text" value="'+datos_compra.direccion+'" name="direccion_envio" id="direccion_envio">'+
								'<input type="hidden" value="Pendiente" name="estado" id="estado">'+
							'</td>');
				});			
			});
		});
	});
}
function confirmar_compra(evento)
{
	evento.preventDefault();
	var datosConfirm = $(this).serialize();
	$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"] ).done(function(data) 
	{
		$('#Lista_compra_actual li').remove();
		var datosA = data.items;		
		$.each(datosA, function(index, datoA) 
		{
			$.ajax({
				data: datosConfirm,
				type: 'POST',
				dataType: 'json',
				url: 'http://new-master.com/smartphuy_confirmBuy.php?mercado='+datoA.mercado+'&factura='+datoA.factura,
				success: function()
				{					
					window.open('codigo_compra.html?codigo_usuario='+getUrlVars()["codigo_usuario"]);
				},
				error: function() 
				{
					navigator.notification.alert("Error Confirmando Compra");
				}
			});
		});
	});
}