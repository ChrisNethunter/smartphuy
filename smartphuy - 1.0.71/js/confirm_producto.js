/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
var datos;
$('#Page_confirm_producto').live('pageshow', function (event)
{
	datosScanner();
	$('#Add_Producto_list').submit(Agregar_Pruducto);
});
function datosScanner()
{
	var codigo = getUrlVars()["codigo"];
	var user = getUrlVars()["codigo_usuario"];
$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"] ).done(function(data) 
	{
		$('#Btn_back_product').attr('href','list_compra.html?codigo_usuario='+user);
		$('#Lista_compra_actual li').remove();
		var datosA = data.items;		
		$.each(datosA, function(index, datoA) 
		{
			$.getJSON(serviceURL + 'smartphuy_datosProducto.php?codigo='+codigo+'&mercado='+datoA.mercado).done(function(data)
			{	
				var codigoHTML;
				datos = data.items;
				if(data.items == 0)
				{
					$('#Content_confirmProducto').html('<h1>Producto No Encontrado</h1>');
					$('#Page_confirm_producto input').remove();
				}
				else
				{
					$.each(datos, function(index, dato) 
					{			
						codigoHTML= '<table align="center">'+
										'<tr>'+
											'<td><img src="'+serviceURL+'/smartphuy_img/productos/'+dato.foto+'" width="60px"></td>'+
											'<td  align="center"><h2>'+dato.nombre+'</h2></td>'+
										'</tr>'+
										'<tr>'+
											'<td colspan="2"  align="center"><p style="text-align:center;">'+dato.descripcion+'</p></td>'+
										'</tr>'+
										'<tr>'+
											'<td  colspan="2"  align="center"><p>Precio: '+dato.precio+' - Cantidad:<input type="number" value="1" name="cantidad_compra" id="cantidad_compra" onChange="calcularTotal();"></p></td>'+
										'</tr>'+
									'</table>' +
									'<input type="hidden" value="'+datoA.factura+'" name="factura" id="factura">'+
									'<input type="hidden" value="'+datoA.mercado+'" name="mercado" id="mercado">'+
									'<input type="hidden" value="'+datoA.codigo_usuario+'" name="codigo_usuario" id="codigo_usuario">'+
									'<input type="hidden" value="'+dato.precio+'" name="precio" id="precio">'+
									'<input type="hidden" value="'+dato.precio+'" name="precioUnit" id="precioUnit">'+
									'<input type="hidden" value="En Proceso" name="estado" id="estado">'+
									'<input type="hidden" value="'+codigo+'" name="codigo_producto" id="codigo_producto">'+
									'<input type="hidden" value="'+dato.cantidad_limite+'" name="cantidad_disponible" id="cantidad_disponible">';
										
						$('#Content_confirmProducto').html(codigoHTML);
					});
				}
			});
		});
	});
}
function Agregar_Pruducto(evento)
{
	evento.preventDefault();
	var datos_formulario = $(this).serialize();
	var cantidad_compra = $('#cantidad_compra').val();
	var user = getUrlVars()["codigo_usuario"];
	//navigator.notification.alert(datos_formulario);
$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+user ).done(function(data) 
	{
		$('#Lista_compra_actual li').remove();
		var datosA = data.items;		
		$.each(datosA, function(index, datoA) 
		{
			if (cantidad_compra > 0 )
			{ 
				if(datoA.factura == 0)
				{					
					//navigator.notification.alert("Datos: "+datos_formulario, null, 'Information');  
					$.ajax({
							data: datos_formulario,
							type: 'post',
							dataType: 'json',
							url: 'http://new-master.com/smartphuy_addfactura.php',
							success: function(data)
							{
								datos = data.items;
								$.each(datos, function(index, dato) 
								{
									//navigator.notification.alert("Entro ajax 1. factura: "+ dato.codigo_factura, null, 'Information');
									$.ajax({
											data: datos_formulario,
											type: 'post',
											dataType: 'json',
											url: 'http://new-master.com/smartphuy_addlista_compra.php?factura='+dato.codigo_factura,
											success: function()
											{
												//navigator.notification.alert("Entro ajax 2"+user+dato.codigo_factura, null, 'Information');
												var codigo = dato.codigo_factura;
												TransitionPage(codigo);
											},
											error: function() 
											{
												navigator.notification.alert("No se pudo añadir el producto", null, 'Information');
											}
										});	
								});
							},
							error: function() 
							{
								navigator.notification.alert("No se pudo añadir el producto", null, 'Information');
							}
						});	
				}
				else
				{	
					$.getJSON(serviceURL + 'smartphuy_datosFactura.php?codigo='+datoA.factura+'&mercado='+datoA.mercado).done(function(data)
					{	
						datos = data.items;
						$.each(datos, function(index, dato) 
						{
							var precio = $('#precio').val();
							var total = parseFloat(dato.total)+parseFloat(precio);
							//navigator.notification.alert("Entro datos total: "+total, null, 'Information');
										  
							$.ajax({
									data: datos_formulario,
									type: 'post',
									dataType: 'json',
									url: 'http://new-master.com/smartphuy_modfactura.php?total='+total+'&codigo='+datoA.factura,
									success: function(jsonObj)
									{
										//navigator.notification.alert("OK Mod", null, 'Information');
										$.ajax({
											data: datos_formulario,
											type: 'post',
											dataType: 'json',
											url: 'http://new-master.com/smartphuy_addlista_compra.php?factura='+datoA.factura,
											success: function()
											{	
												//navigator.notification.alert("Entro ajax 2", null, 'Information');
												window.open('list_compra.html?codigo_usuario='+user);
											},
											error: function() 
											{
												navigator.notification.alert("No se pudo añadir el producto", null, 'Information');
											}
										});
									},
									error: function() 
									{
										navigator.notification.alert("No se pudo añadir el producto", null, 'Information');
									}
								});
						});
					}).fail(function()
					{
						navigator.notification.alert("Error al buscar factura", null, 'Information');
					});
				}
			}	
			else
			{
				navigator.notification.alert("La cantidad del producto debe ser mayor a cero", null, 'Information');
			}
		});
	});
}
function calcularTotal()
{
	var cantidad_compra = $('#cantidad_compra').val();
	if(parseFloat($('#cantidad_compra').val()) <= parseFloat($('#cantidad_disponible').val()))
	{
		var valor_Unit = $('#precioUnit').val();
		var calculado = parseFloat(valor_Unit) * parseFloat(cantidad_compra);
		//navigator.notification.alert(calculado, null, 'Information');
		document.getElementById('precio').value=calculado;
	}
	else
	{
		navigator.notification.alert('Supera la Cantidad Limite: '+$('#cantidad_disponible').val(), null, 'Information');
		var valor_Unit = $('#precioUnit').val();
		var calculado = parseFloat(valor_Unit) * parseFloat($('#cantidad_disponible').val());
		document.getElementById('cantidad_compra').value=$('#cantidad_disponible').val();
		document.getElementById('precio').value=calculado;	
	}
}
function TransitionPage(codigo_factura)
{	
	var user = getUrlVars()["codigo_usuario"];	
	var userF=user.split("#");
	//navigator.notification.alert(userF[0], null, 'Information');
	var cod = userF[0];
	$.getJSON(serviceURL+'smartphuy_addFac_Act.php?codigo_usuario='+cod+'&factura='+codigo_factura).done(function()
		{			
			window.open('list_compra.html?codigo_usuario='+cod);
		}).fail(function()
		{
			navigator.notification.alert('Error al Añadir TempFac', null, 'Information');
		});
}
function getUrlVars() 
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}