/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
function verificarListaCompra()
{
$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"] ).done(function(data) 
	{
		$('#Lista_compra_actual li').remove();
		var datosA = data.items;		
		$.each(datosA, function(index, datoA) 
		{
			if(datoA.factura == 0)
				{
					if(parseFloat($('#Total_Compra_Actual').val()) == 0)
					{
						navigator.notification.alert("Lista de Compra Vacia");
					}
					else
					{
						$.getJSON(serviceURL + 'smartphuy_datosFactura.php?codigo='+datoA.factura+'&mercado='+datoA.mercado).done(function(data)
						{	
							var datosFin = data.items;
							$.each(datosFin, function(index, datoFin) 
							{
								if(datoFin.total != $('#Total_Compra_Actual').val())
								{
									$.getJSON(serviceURL + 'smartphuy_modfacturaTotal.php?factura='+datoA.factura+'&mercado='+datoA.mercado+'&total='+$('#Total_Compra_Actual').val()).done(function()
									{
										window.open('confirm_compra.html?codigo_usuario='+getUrlVars()["codigo_usuario"]);
									});
								}
								else
								{
									window.open('confirm_compra.html?codigo_usuario='+getUrlVars()["codigo_usuario"]);
								}
							});
						});
					}
				}
				else
				{
					window.open('confirm_compra.html?codigo_usuario='+getUrlVars()["codigo_usuario"]);
				}
		});
	});
}
function externo_stado()
{
	/*$.post('https://www.google.com.co',{usuario: getUrlVars()["codigo_usuario"]},function()
	{
		window.open('https://www.google.com.co');
	});*/
}
$('#Page_codigo_Compra').live('pageshow', function (event)
{
	$('#salirCodigo').attr('onClick','deleteFactAct('+getUrlVars()["codigo_usuario"]+');');
});
$('#Page_consejo').live('pageshow', function (event)
{
	$('#URL_Consejo').attr('href','list_mercado.html?codigo_usuario='+getUrlVars()["codigo_usuario"]+');');
});


function deleteFactAct(codigoDel)
{
	//navigator.notification.alert(codigoDel);
	$.getJSON('http://new-master.com/smartphuy_addFac_Act.php?codigo_usuario='+codigoDel+'&factura=0').done(function()
	{
		window.open('list_mercado.html?codigo_usuario='+codigoDel);
	});
}
function deleteSesion(codigo)
{
	$.getJSON(serviceURL + 'smartphuy_delUser_Act.php?codigo_usuario='+codigo);
}
function getUrlVars() 
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
