/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
var datos;
$('#Page_histCompra').live('pageshow', function (event)
{	
	/*
	$('#perfilUserHCompra').attr('href','perfil.html?codigo_usuario='+codigoHCom);
	$('#comprasUserHCompra').attr('href','compras.html?codigo_usuario='+codigoHCom);
	$('#mercadosUserHCompra').attr('href','list_mercado.html?codigo_usuario='+codigoHCom);
	$('#salirSesionHCompra').attr('onClick','deleteSesion('+codigoHCom+');');
	*/
	listaCompra();
});
function listaCompra()
{
	var mercado = getUrlVars()["mercado"];
	$.getJSON(serviceURL + 'smartphuy_listCompras.php?mercado='+mercado+'&user='+getUrlVars()["codigo_usuario"]).done(function(data)
	{	
		var db = openDatabase('Smartempdb', '1.0', 'Test DB', 1000000);
		db.transaction(function (tx) {
			tx.executeSql('CREATE TABLE IF NOT EXISTS HistCompras (estado, fecha, total, factura)');
			tx.executeSql('DELETE FROM HistCompras');
		});
		$('#lista_histCompra li').remove();
		compras = data.items;
		//navigator.notification.alert(mercados);		
		$.each(compras, function(index, compra) 
		{
			$('#lista_histCompra').append('<li data-role="listview">'+
                    '<a href="hist_list_compra.html?factura='+compra.codigo_factura+'&user='+getUrlVars()["codigo_usuario"]+'&mercado='+mercado+'" data-transition="slide">'+
                        '<h2>Total: '+compra.total+'</h2>'+
                        '<p align="right">Estado: '+compra.estado+' - Fecha: '+compra.fecha+'</p>'+
                    '</a>'+
                '</li>');
				db.transaction(function (tx) {
					tx.executeSql('CREATE TABLE IF NOT EXISTS HistCompras (estado, fecha, total, factura)');
					tx.executeSql('INSERT INTO HistCompras (estado, fecha, total, factura) VALUES ("'+compra.estado+'", "'+compra.fecha+'",'+compra.total+', '+compra.codigo_factura+')');
				});
		});
		$('#lista_histCompra').listview('refresh');
		//tempHistProductos();///////////////////////////////////////////
	});
	$.getJSON(serviceURL + 'smartphuy_listCompras.php?mercado='+mercado+'&user='+getUrlVars()["codigo_usuario"]).fail(function()
	{
		//navigator.notification.alert('Entro 2');
			var db = openDatabase('Smartempdb', '1.0', 'Test DB', 1000000);
			db.transaction(function (tx) {
			  tx.executeSql('SELECT * FROM HistCompras', [], function (tx, results) {
			   var len = results.rows.length, i;
			   for (i = 0; i < len; i++){
				   //navigator.notification.alert('mercadoActual'+results.rows.item(i).id);
				   $('#lista_histCompra').append('<li data-role="listview">'+
                    '<a href="hist_list_compra.html?factura='+results.rows.item(i).factura+'&user='+getUrlVars()["codigo_usuario"]+'&mercado='+mercado+'" data-transition="slide">'+
                        '<h2>Total: '+results.rows.item(i).total+'</h2>'+
                        '<p align="right">Estado: '+results.rows.item(i).estado+' - Fecha: '+results.rows.item(i).fecha+'</p>'+
                    '</a>'+
                '</li>');
				$('#lista_histCompra').listview('refresh');
			   }
			 }, null);
			});
	});
	var codigoHCom= getUrlVars()['codigo_usuario'];
	//navigator.notification.alert(codigoHCom);
	document.getElementById('perfilUserHCompra').setAttribute('href','perfil.html?codigo_usuario='+codigoHCom);
	document.getElementById('comprasUserHCompra').setAttribute('href','compras.html?codigo_usuario='+codigoHCom);
	document.getElementById('mercadosUserHCompra').setAttribute('href','list_mercado.html?codigo_usuario='+codigoHCom);
	document.getElementById('salirSesionHCompra').setAttribute('onClick','deleteSesion('+codigoHCom+');');
	$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+codigoHCom).done(function(data)
	{
		var datosU = data.items;
		$.each(datosU, function(index, datoU) 
			{
				//navigator.notification.alert(datoU.nombre+' '+datoU.apellido);
				$('#User_ActivoHCompra').html(datoU.nombre+' '+datoU.apellido);
			});
	});
}
function getUrlVars() 
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
