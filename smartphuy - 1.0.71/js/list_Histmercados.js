/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
var ofertas;
$('#page_Histmercados').live('pageshow', function(event) 
{		
	/*
	$('#perfilUserHMer').attr('href','perfil.html?codigo_usuario='+codigoHMer);
	$('#comprasUserHMer').attr('href','compras.html?codigo_usuario='+codigoHMer);
	$('#mercadosUserHMer').attr('href','list_mercado.html?codigo_usuario='+codigoHMer);
	$('#salirSesionHMer').attr('onClick','deleteSesion('+codigoHMer+');');
	*/
	ListarHistoriMercados();	
});
function ListarHistoriMercados() 
{
	$.getJSON(serviceURL + 'smartphuy_mercados.php').done(function(data) 
	{
		$('#lista_hisMercados li').remove();
		mercados = data.items;
		//navigator.notification.alert(mercados);		
		$.each(mercados, function(index, mercado) 
		{
			$('#lista_hisMercados').append('<li data-role="listview">'+
                    '<a href="hist_compra.html?mercado='+mercado.codigo_mercado+'&codigo_usuario='+getUrlVars()["codigo_usuario"]+'" data-transition="slide">'+
                        '<img src="'+serviceURL+'smartphuy_img/'+mercado.imagen+'" id="img_mercado" height="100px">'+
                        '<h1>'+mercado.nombre+'</h1>'+
                        '<p>'+mercado.frase+'</p>'+
                   ' </a>'+
                '</li>');
		});
		$('#lista_hisMercados').listview('refresh');
	});
	$.getJSON(serviceURL + 'smartphuy_mercados.php').fail(function()
	{
		//navigator.notification.alert('Entro 2');
		$('#lista_hisMercados').html('<li data-role="list-divider" data-theme="a" style="text-align:center; opacity:0.5; color:#000; padding-top:20px;"><p><img src="themes/images/icons-png/alert-black.png">No hay conexión a internet</p></li>');
		window.open('hist_compra.html');			
	});
	var codigoHMer= getUrlVars()['codigo_usuario'];
	//navigator.notification.alert(codigoHMer);
	document.getElementById('perfilUserHMer').setAttribute('href','perfil.html?codigo_usuario='+codigoHMer);
	document.getElementById('comprasUserHMer').setAttribute('href','compras.html?codigo_usuario='+codigoHMer);
	document.getElementById('mercadosUserHMer').setAttribute('href','list_mercado.html?codigo_usuario='+codigoHMer);
	document.getElementById('salirSesionHMer').setAttribute('onClick','deleteSesion('+codigoHMer+');');
	$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+codigoHMer).done(function(data)
	{
		var datosU = data.items;
		$.each(datosU, function(index, datoU) 
			{
				//navigator.notification.alert(datoU.nombre+' '+datoU.apellido);
				$('#User_ActivoHMer').html(datoU.nombre+' '+datoU.apellido);
			});
	});
}
function getUrlVars() 
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}