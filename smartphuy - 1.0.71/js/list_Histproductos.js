/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
var productos;
$('#Page_HistListproductos').live('pageshow', function (event)
{		
	/*
	$('#perfilUserHLCompra').attr('href','perfil.html?codigo_usuario='+codigoHLCom);
	$('#comprasUserHLCompra').attr('href','compras.html?codigo_usuario='+codigoHLCom);
	$('#mercadosUserHLCompra').attr('href','list_mercado.html?codigo_usuario='+codigoHLCom);
	$('#salirSesionHLCompra').attr('onClick','deleteSesion('+codigoHLCom+');');
	*/
	listaHistProductos();
});
function listaHistProductos()
{	
	var mercado = getUrlVars()["mercado"];
	var factura = getUrlVars()["factura"];
	var user = getUrlVars()["user"];
	$('#Btn_Recompra').attr('onClick','recompra('+factura+');');
	//navigator.notification.alert(mercado+factura+user);
	$.getJSON(serviceURL + 'smartphuy_listHistproductos.php?mercado='+mercado+'&user='+user+'&factura='+factura).done(function(data)
	{	
		$('#list_Histproductos li').remove();
		productos = data.items;
		//navigator.notification.alert(productos);		
		$.each(productos, function(index, producto) 
		{			
			$('#list_Histproductos').append('<li data-role="listview">'+
								'<table id="tabla_cantidad">'+
									'<tr>'+
										'<td rowspan="2"><img src="'+serviceURL+'smartphuy_img/productos/'+producto.foto+'" width="60px"></td>'+
										'<td colspan="4"><h1>'+producto.nombre+'</h1></td>'+
									'</tr>'+
									'<tr>'+
										'<td><p>Cantidad: '+producto.cantidad+' - Subtotal: '+producto.subtotal+'</p></td>'+
									'</tr>'+
								'</table>'+
							'</li>');
		});
		$('#list_Histproductos').listview('refresh');
	}).fail(function()
	{
		//navigator.notification.alert('Entro 2');
		$('#list_Histproductos').append('<li data-role="list-divider" data-theme="a" style="text-align:center; opacity:0.5; color:#000; padding-top:20px;"><p><img src="themes/images/icons-png/alert-black.png">No hay conexión a internet</p></li>');
			var db = openDatabase('Smartempdb', '1.0', 'Test DB', 1000000);
			db.transaction(function (tx) {
			  tx.executeSql('SELECT * FROM HistProductos WHERE factura='+factura, [], function (tx, results) {
			   var len = results.rows.length, i;
			   for (i = 0; i < len; i++){
				   //navigator.notification.alert('mercadoActual'+results.rows.item(i).id);
				   $('#list_Histproductos').append('<li data-role="listview">'+
								'<table id="tabla_cantidad">'+
									'<tr>'+
										'<td rowspan="2"><img src="img/standar.jpg" width="60px"></td>'+
										'<td colspan="4"><h1>'+results.rows.item(i).name+'</h1></td>'+
									'</tr>'+
									'<tr>'+
										'<td><p>Cantidad: '+results.rows.item(i).cantidad+' - Subtotal: '+results.rows.item(i).subtotal+'</p></td>'+
									'</tr>'+
								'</table>'+
							'</li>');
				$('#list_Histproductos').listview('refresh');
			   }
			 }, null);
			});
	});
	var codigoHLCom= getUrlVars()['user'];
	//navigator.notification.alert(codigoHLCom);
	document.getElementById('perfilUserHLCompra').setAttribute('href','perfil.html?codigo_usuario='+codigoHLCom);
	document.getElementById('comprasUserHLCompra').setAttribute('href','compras.html?codigo_usuario='+codigoHLCom);
	document.getElementById('mercadosUserHHLCompra').setAttribute('href','list_mercado.html?codigo_usuario='+codigoHLCom);
	document.getElementById('salirSesionHLCompra').setAttribute('onClick','deleteSesion('+codigoHLCom+');');
	$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+codigoHLCom).done(function(data)
	{
		var datosU = data.items;
		$.each(datosU, function(index, datoU) 
			{
				//navigator.notification.alert(datoU.nombre+' '+datoU.apellido);
				$('#User_ActivoHLCompra').html(datoU.nombre+' '+datoU.apellido);
			});
	});
}
function recompra(CodFac)
{
	var mercado = getUrlVars()["mercado"];
	$.getJSON(serviceURL + 'smartphuy_addMer_Act.php?codigo_usuario='+getUrlVars()["user"]+'&mercado='+mercado);
	var factura = CodFac;
	var user = getUrlVars()["user"];
$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+getUrlVars()["user"] ).done(function(data) 
	{
		$('#Lista_compra_actual li').remove();
		var datosA = data.items;		
		$.each(datosA, function(index, datoA) 
		{	
			//navigator.notification.alert(mercado+'-'+factura+'-'+user);
			if(datoA.factura == 0)
			{ 			
				$.getJSON(serviceURL + 'smartphuy_recompraFactura.php?mercado='+mercado+'&codigo_usuario='+user+'&precio=0&estado=En Proceso').done(function(data)
					{
					var datos = data.items;
					$.each(datos, function(index, dato) 
					{											
						$.getJSON(serviceURL+'smartphuy_addFac_Act.php?codigo_usuario='+getUrlVars()["user"]+'&factura='+dato.codigo_factura).done(function()
						{
						relista(dato.codigo_factura,CodFac);
						});
					});
					});					
			}
			else
			{
				relista(datoA.factura,CodFac);
			}
		});
	});
}
function relista(codFac,datoFac)
{
	$.getJSON(serviceURL + 'smartphuy_listHistproductos.php?mercado='+getUrlVars()["mercado"]+'&user='+getUrlVars()["user"]+'&factura='+datoFac).done(function(data)
	{			
		productos = data.items;
		//navigator.notification.alert(productos);		
		$.each(productos, function(index, producto) 
		{	
				//navigator.notification.alert('Total:'+total, null, 'Information');						
				$.getJSON(serviceURL + 'smartphuy_reAddLista.php?mercado='+getUrlVars()["mercado"]+'&factura='+codFac+'&precio='+producto.subtotal+'&cantidad_compra='+producto.cantidad+'&codigo_producto='+producto.codigo_producto);
		});
	}).fail(function()
	{
		navigator.notification.alert("Tiene una Compra Activa");
		window.open('list_compra.html');
	});
	modReFac(codFac,datoFac);
}
function modReFac(codFac,datoFac)
{
	//navigator.notification.alert("Total: "+total);
	$.getJSON(serviceURL + 'smartphuy_datosFactura.php?mercado='+getUrlVars()["mercado"]+'&codigo='+datoFac ).done(function(data) 
	{
		var datos_A = data.items;	
		$.each(datos_A, function(index, dato_A) 
		{
			$.getJSON(serviceURL + 'smartphuy_datosFactura.php?mercado='+getUrlVars()["mercado"]+'&codigo='+codFac ).done(function(data) 
			{
				var datos_B = data.items;	
				$.each(datos_B, function(index, dato_B) 
				{
					var total = parseFloat(dato_A.total)+parseFloat(dato_B.total);
					$.getJSON(serviceURL+'smartphuy_reModfactura.php?total='+total+'&factura='+codFac+'&estado=En Proceso&mercado='+getUrlVars()["mercado"]).done(function()
					{
						window.open('list_compra.html?mercado='+getUrlVars()["mercado"]+'&codigo_usuario='+getUrlVars()["user"]);
					});
				});
			});
		});
	});
}
function getUrlVars() 
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}