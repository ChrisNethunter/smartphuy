/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
var productos;
var factura
$('#Page_list_compra_actual').live('pageshow', function(event) 
{		
	/*
	$('#perfilUserLCompra').attr('href','perfil.html?codigo_usuario='+codigoCom);
	$('#comprasUserLCompra').attr('href','compras.html?codigo_usuario='+codigoCom);
	$('#mercadosUserLCompra').attr('href','list_mercado.html?codigo_usuario='+codigoCom);
	$('#salirSesionLCompra').attr('onClick','deleteSesion('+codigoCom+');');
	*/
	ListarCompraActual();
	definirUrl()	
});
function ListarCompraActual() 
{
	$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"] ).done(function(data) 
	{
		$('#Lista_compra_actual li').remove();
		var datos = data.items;		
		$.each(datos, function(index, dato) 
		{
			$.getJSON(serviceURL + 'smartphuy_listHistproductos.php?mercado='+ dato.mercado+'&factura='+dato.factura+'&user='+dato.codigo_usuario ).done(function(data) 
			{
				$('#Lista_compra_actual li').remove();
				productos = data.items;
				var totalCompra = 0;
				//navigator.notification.alert(mercados);		
				$.each(productos, function(index, producto) 
				{
					totalCompra = parseFloat(totalCompra)+parseFloat(producto.subtotal);
					
					$('#Lista_compra_actual').append('<li data-role="listview" id="'+producto.codigo_producto+'">'+
							'<table id="tabla_cantidad"  width="95%" align="center">'+
								'<tr>'+
									'<td rowspan="2"><img src="'+serviceURL+'smartphuy_img/productos/'+producto.foto+'" width="60px"></td>'+
									'<td colspan="4" width="74"><h1>'+producto.nombre+'</h1></td>'+
									'<td rowspan="2"><a href="#" onclick="Delete_Producto('+producto.codigo+');"><img src="img/del-producto2.png" title="Borrar" width="60px"></a></td>'+
								'</tr>'+
								'<tr>'+
									'<td>Cantidad: </td>'+
									'<td><input id="'+producto.codigo+'" name="'+producto.codigo+'" type="number" value="'+producto.cantidad+'" size="8" onChange="Mod_cantProducto('+producto.codigo_producto+', '+producto.codigo+');"></td>'+
									'<td></td>'+
								'</tr>'+
							'</table>'+
						'</li>');
				});
				document.getElementById('Total_Compra_Actual').value=totalCompra;
				$('#Lista_compra_actual').listview('refresh');	
			});
		});
	});
}
function definirUrl()
{	
	var codigoCom= getUrlVars()['codigo_usuario'];
	//navigator.notification.alert(codigoCom);
	document.getElementById('perfilUserLCompra').setAttribute('href','perfil.html?codigo_usuario='+codigoCom);
	document.getElementById('comprasUserLCompra').setAttribute('href','compras.html?codigo_usuario='+codigoCom);
	document.getElementById('mercadosUserLCompra').setAttribute('href','list_mercado.html?codigo_usuario='+codigoCom);
	document.getElementById('salirSesionLCompra').setAttribute('onClick','deleteSesion('+codigoCom+');');
	$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+codigoCom).done(function(data)
	{
		var datosU = data.items;
		$.each(datosU, function(index, datoU) 
			{
				//navigator.notification.alert(datoU.nombre+' '+datoU.apellido);
				$('#User_ActivoLCompra').html(datoU.nombre+' '+datoU.apellido);
			});
	});
}
function scanCode()
{
	window.plugins.barcodeScanner.scan(
		function(result){						
			//document.getElementById('data_text').value=result.text;
			var codigo = result.text;
			//alert(result.text);
			verificarProducto(codigo);
			if(result.cancelled==true){
				window.open('list_compra.html');
				alert('Scaneo Cancelado');
			}
		}, 
		function(error){
			alert("Scan failed");
		}
	);
}
function verificarProducto(CodResult)
{
	//navigator.notification.alert('Entro: '+CodResult);
$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"] ).done(function(data) 
	{
		$('#Lista_compra_actual li').remove();
		var datos = data.items;		
		$.each(datos, function(index, dato) 
		{
			if(dato.factura !=0)
			{
				//alert('Entro');
				$.getJSON(serviceURL + 'smartphuy_veriProducto.php?codigo='+CodResult+'&mercado='+dato.mercado+'&factura='+dato.factura).done(function(data)
				{	
					var datos_list = data.items;
					//navigator.notification.alert(datos_list);
					$.each(datos_list, function(index, dato_list) 
					{
					//alert(data.items+' - datos_list'+datos_list);
						if(String(dato_list.codigo_producto) != String(CodResult))///////////////////////////
						{
							window.open('confirm_producto.html?codigo='+CodResult+'&codigo_usuario='+dato.codigo_usuario);
						}
						else
						{
							window.open('list_compra.html?codigo_usuario='+dato.codigo_usuario);
							navigator.notification.alert('!El Producto ya esta en la lista de compra¡');						
						}
					});
					if(data.items == 0)
					{
						window.open('confirm_producto.html?codigo='+CodResult+'&codigo_usuario='+dato.codigo_usuario);
					}
				});
			}
			else
			{
				window.open('confirm_producto.html?codigo='+CodResult+'&codigo_usuario='+dato.codigo_usuario);	
			}
		});
	});
}
function Delete_Producto(codigo)
{
$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"] ).done(function(data) 
	{
		$('#Lista_compra_actual li').remove();
		var datosA = data.items;		
		$.each(datosA, function(index, datoA) 
		{
			$.getJSON(serviceURL + 'smartphuy_datosProductoList.php?mercado='+datoA.mercado+'&factura='+datoA.factura+'&codigo='+codigo ).done(function(data)
			{
				var productos = data.items;
				//navigator.notification.alert(mercados);		
				$.each(productos, function(index, producto) 
				{
					var descuento = producto.subtotal;
					$.getJSON(serviceURL + 'smartphuy_delProducto.php?mercado='+ datoA.mercado+'&factura='+datoA.factura+'&codigo='+codigo );
					$.getJSON(serviceURL + 'smartphuy_datosFactura.php?codigo='+datoA.factura+'&mercado='+datoA.mercado).done(function(data)
					{	
						var datos = data.items;
						$.each(datos, function(index, dato) 
						{
							var total = parseFloat(dato.total)-parseFloat(descuento);
							$.getJSON(serviceURL + 'smartphuy_modfacturaTotal.php?factura='+datoA.factura+'&mercado='+datoA.mercado+'&total='+total);	
							$.getJSON(serviceURL + 'smartphuy_datosFactura.php?codigo='+datoA.factura+'&mercado='+datoA.mercado).done(function(data)
							{	
								var datosFin = data.items;
								$.each(datosFin, function(index, datoFin) 
								{
									if(parseFloat(datoFin.total) <= 0)
									{
										$.getJSON(serviceURL + 'smartphuy_delFactura.php?mercado='+ datoA.mercado+'&factura='+datoA.factura);
										$.getJSON(serviceURL+'smartphuy_addFac_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"]+'&factura=0').done(function()
										{
											ListarCompraActual();
										});
									}
									else
									{
										ListarCompraActual();
									}
								});
							});
						});
					});
				});
			});
		});
	});
}
function Mod_cantProducto(producto, codigo)
{
	var cantidad =  $('#'+codigo+'').val(); //document.getElementById().getAttribute('value');
	if(cantidad != 0)
	{	
		$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"] ).done(function(data) 
		{
			$('#Lista_compra_actual li').remove();
			var datosA = data.items;		
			$.each(datosA, function(index, datoA) 
			{
				$.getJSON(serviceURL + 'smartphuy_datosProducto.php?codigo='+producto+'&mercado='+datoA.mercado).done(function(data)
				{
					var datos = data.items;		
					$.each(datos, function(index, dato) 
					{
						if(parseFloat(cantidad) > parseFloat(dato.cantidad_limite))
						{
							navigator.notification.alert('Supera la Cantidad Limite: '+dato.cantidad_limite);
							cantidad = dato.cantidad_limite;
						}
						var subtotal = parseFloat(dato.precio)*parseFloat(cantidad);
						$.getJSON(serviceURL + 'smartphuy_datosProductoList.php?mercado='+datoA.mercado+'&factura='+datoA.factura+'&codigo='+codigo ).done(function(data)
						{
							var productos = data.items;						
							$.each(productos, function(index, producto) 
							{
								$.getJSON(serviceURL+'smartphuy_datosFactura.php?codigo='+datoA.factura+'&mercado='+datoA.mercado).done(function(data)
								{	
									var datosFin = data.items;
									$.each(datosFin, function(index, datoFin) 
									{
										var total = (parseFloat(datoFin.total)-parseFloat(producto.subtotal))+parseFloat(subtotal);
										$.getJSON(serviceURL + 'smartphuy_ModProductoList.php?mercado='+datoA.mercado+'&subtotal='+subtotal+'&codigo='+codigo+'&cantidad='+cantidad);
										$.getJSON(serviceURL + 'smartphuy_modfacturaTotal.php?factura='+datoA.factura+'&mercado='+datoA.mercado+'&total='+total);
										ListarCompraActual();
									});
								});
							});
						});
					});
				});
			});
		});
	}
	else
	{
		ListarCompraActual();
		navigator.notification.alert('La cantidad debe ser mayor a Cero (0)');
	}
}

function getUrlVars() 
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}