/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
var ofertas;
$('#page_mercado').live('pageshow', function(event) 
{		
	/*$('#perfilUserMer').attr('href','perfil.html?codigo_usuario='+codigoMer);
	$('#comprasUserMer').attr('href','compras.html?codigo_usuario='+codigoMer);
	$('#mercadosUserMer').attr('href','list_mercado.html?codigo_usuario='+codigoMer);
	$('#salirSesionMer').attr('onClick','deleteSesion('+codigoMer+');');*/
	var codigoMer = getUrlVars()['codigo_usuario'];
	document.getElementById('perfilUserMer').setAttribute('href','perfil.html?codigo_usuario='+codigoMer);
	document.getElementById('comprasUserMer').setAttribute('href','compras.html?codigo_usuario='+codigoMer);
	document.getElementById('mercadosUserMer').setAttribute('href','list_mercado.html?codigo_usuario='+codigoMer);
	document.getElementById('salirSesionMer').setAttribute('onClick','deleteSesion('+codigoMer+');');
	ListarMercados();
});
function ListarMercados() 
{
	var codigo_usuario  = getUrlVars()["codigo_usuario"];
	$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+codigo_usuario).done(function(data)
	{
		var datosU = data.items;
		$.each(datosU, function(index, datoU) 
			{
				//navigator.notification.alert(datoU.nombre+' '+datoU.apellido);
				$('#User_ActivoMer').html(datoU.nombre+' '+datoU.apellido);
			});
	});	
	$.getJSON(serviceURL + 'smartphuy_mercados.php').done(function(data) 
	{
		$('#lista_mercados li').remove();
		mercados = data.items;
		//navigator.notification.alert(mercados);		
		$.each(mercados, function(index, mercado) 
		{
			$('#lista_mercados').append('<li data-role="listview">'+
			   '<a href="list_compra.html?mercado='+mercado.codigo_mercado+'&codigo_usuario='+codigo_usuario+'" data-transition="slide" onClick="codMercado('+mercado.codigo_mercado+');">'+
					'<img src="'+serviceURL+'smartphuy_img/'+mercado.imagen+'" id="img_mercado" height="100px">'+
					'<h1>'+mercado.nombre+'</h1>'+
					'<p>'+mercado.frase+'</p>'+
			   ' </a>'+
			'</li>');
		});
		$('#lista_mercados').listview('refresh');	
	}).fail(function()
		{
			$('#lista_mercados').html('<li data-role="list-divider" data-theme="a" style="text-align:center; opacity:0.5; color:#000; padding-top:20px;"><p><img src="themes/images/icons-png/alert-black.png">No hay conexión a internet</p></li>');
		}
	);
	
}
function codMercado(codigo)
{
	$.getJSON(serviceURL + 'smartphuy_datosUser_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"] ).done(function(data) 
	{
		$('#Lista_compra_actual li').remove();
		var datos = data.items;		
		$.each(datos, function(index, dato) 
		{
			if(dato.mercado == codigo)
			{
				$.getJSON(serviceURL + 'smartphuy_addMer_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"]+'&mercado='+codigo);
			}
			else
			{
				$.getJSON(serviceURL + 'smartphuy_addMer_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"]+'&mercado='+codigo);
				$.getJSON(serviceURL+'smartphuy_addFac_Act.php?codigo_usuario='+getUrlVars()["codigo_usuario"]+'&factura=0');
			}
		});
	});
}
function getUrlVars() 
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}