/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
$('#PageModPass').live('pageshow', function (event)
{
	$("#form_mod_pass").submit(ModPassword);
	var codigoPass = getUrlVars()["codigo_usuario"];
	$("#codigo").attr('value',codigoPass);
	$('#BtonCancelModUser').attr('href','perfil.html?codigo_usuario='+codigoPass);
});
function verificardatos()
{
	if($('#last_Contrasena').val() == 0)
	{
		$('#last_Contrasena').attr('style','box-shadow: 0 0 5px 5px #FF0000');
	}
	else
	{
		$('#last_Contrasena').removeAttr('style');
	}
}
function verificarcontrasena()
{
	if($('#new_Contrasena').val() == 0)
	{
		$('#new_Contrasena').attr('style','box-shadow: 0 0 5px 5px #FF0000');
		$('#Btn_modPass').attr('disabled');
	}
	else
	{
		$('#new_Contrasena').removeAttr('style');
		$('#Btn_modPass').removeAttr('disabled');
	}
	
	if( String($('#rep_Contrasena').val()) != String($('#new_Contrasena').val()))
	{
		$('#rep_Contrasena').attr('style','box-shadow: 0 0 5px 5px #FF0000');
		$('#Btn_modPass').attr('disabled');
	}
	else
	{
		$('#rep_Contrasena').removeAttr('style');
		$('#Btn_modPass').removeAttr('disabled');
	}
}
function ModPassword(evento)
{
	evento.preventDefault();
	
	var last_C = $('#last_Contrasena').val();
	var new_C = $('#new_Contrasena').val();
	var rep_C = $('#rep_Contrasena').val();
	//navigator.notification.alert('cedula:'+cedula+ 'nombre:'+nombre+'apellido:'+apellido+'correo:'+correo+'direccion:'+direccion+'celular:'+celular+'contraseña:'+contrasena);
	
	if ( last_C.length > 0 && new_C.length > 0 &&  rep_C.length > 0 )
	{ 
		if( String(rep_C) == String(new_C))
		{
			var datos;
			$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+getUrlVars()["codigo_usuario"]).done(function(data)
			{	
				datos = data.items;
				//navigator.notification.alert(datos);
				$.each(datos, function(index, dato) 
				{
					if(String(last_C) == String(dato.password))
					{
						var datos_formulario = $(this).serialize();  
						$.ajax({
							data: datos_formulario,
							type: 'post',
							dataType: 'json',
							url: 'http://new-master.com/smartphuy_modPass.php',
							success: function(jsonObj)
							{
								window.open('perfil.html?codigo_usuario='+getUrlVars()['codigo_usuario']);
								navigator.notification.alert("Se ha Modificado Correctamente", null, 'Information');
							},
							error: function() 
							{
								navigator.notification.alert("Su informacion no se ha podido Modificar", null, 'Information');
							}
						});
					}
					else
					{
						navigator.notification.alert("Contraseña Actual Incorrecta", null, 'Information');
						window.open('perfil.html?codigo_usuario='+getUrlVars()['codigo_usuario']);
					}
				});
			});
		}
		else
		{
			navigator.notification.alert("La nueva Contraseña NO Coincide", null, 'Information');
		}
	}
	
	else
	{
		navigator.notification.alert("Campos INCOMPLETOS", null, 'Information');
	}
}