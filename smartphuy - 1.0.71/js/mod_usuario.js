/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
$('#Page_Mod_Perfil').live('pageshow', function (event)
{
	datosModUsuario();
	$('#BtonCancelModUser').attr('href','perfil.html?codigo_usuario='+getUrlVars()["codigo_usuario"]);
	$("#form_mod_perfil").submit(ModUsuario);
});
function datosModUsuario()
{
	var datos;
	$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+getUrlVars()["codigo_usuario"]).done(function(data)
	{	
		datos = data.items;
		//navigator.notification.alert(datos);
		$.each(datos, function(index, dato) 
		{	
			document.getElementById('codigo').value=dato.codigo_usuario;
			document.getElementById('modnombre').value=dato.nombre;
			document.getElementById('modapellido').value=dato.apellido;
			document.getElementById('modcorreo').value=dato.email;
			document.getElementById('moddireccion').value=dato.direccion;
			document.getElementById('modcelular').value=dato.celular;
			if(dato.tel_fijo != 0)
			{
				document.getElementById('modtelefono').value=dato.tel_fijo;
			}			
		});
	});
}
function ModUsuario(evento)
{ 
	evento.preventDefault();
	var cedula = getUrlVars()['codigo_usuario'];
	var nombre = $('#modnombre').val();
	var apellido = $('#modapellido').val();
	var correo = $('#modcorreo').val();
	var direccion = $('#moddireccion').val();
	var telefono = $('#modtelefono').val();
	var celular = $('#modcelular').val();
	//navigator.notification.alert('cedula:'+cedula+ 'nombre:'+nombre+'apellido:'+apellido+'correo:'+correo+'direccion:'+direccion+'celular:'+celular+'contraseña:'+contrasena);
	
	if(telefono == 0)
	{
		telefono=0;
	}
	
	if ( nombre.length > 0 && apellido.length > 0 &&  correo.length > 0 && direccion.length > 0 && celular.length > 0)
	{ 
	//navigator.notification.alert("telefono"+telefono+'celular'+celular, null, 'Information');
		var datos_formulario = $(this).serialize();  
		$.ajax({
					data: datos_formulario,
					type: 'post',
					dataType: 'json',
					url: 'http://new-master.com/smartphuy_modUsuario.php?telefono='+telefono+'&modcelular='+celular,
					success: function(jsonObj)
					{
						window.open('perfil.html?codigo_usuario='+cedula);
						navigator.notification.alert("Se ha Modificado Correctamente", null, 'Information');
					},
					error: function() 
					{
						navigator.notification.alert("Su informacion no se ha podido Modificar", null, 'Information');
					}
				});					
	}
	
	else
	{
		navigator.notification.alert("Su informacion esta INCOMPLETA", null, 'Information');
	}
	
}