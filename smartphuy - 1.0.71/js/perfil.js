/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
var serviceURL = "http://new-master.com/";
var datos;
$('#Page_Perfil').live('pageshow', function (event)
{	
	/*
	$('#perfilUserPer').attr('href','perfil.html?codigo_usuario='+codigoPer);
	$('#comprasUserPer').attr('href','compras.html?codigo_usuario='+codigoPer);
	$('#mercadosUserPer').attr('href','list_mercado.html?codigo_usuario='+codigoPer);
	$('#salirSesionPer').attr('onClick','deleteSesion('+codigoPer+');');
	*/	
	datosUsuario();
	$('#BtonModPerfil').attr('href','mod_perfil.html?codigo_usuario='+getUrlVars()['codigo_usuario']);
	$('#BtonModContra').attr('href','mod_pass.html?codigo_usuario='+getUrlVars()['codigo_usuario']);
});
function datosUsuario()
{
	$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+getUrlVars()["codigo_usuario"]).done(function(data)
	{	
		var codigoHTML;
		datos = data.items;
		$.each(datos, function(index, dato) 
		{			
			codigoHTML= '<tr align="center">'+
							'<td colspan="2">'+dato.nombre+' '+dato.apellido+'</td>'+
						'</tr>'+
						'<tr align="center">'+
							'<td>Correo: </td><td>'+dato.email+'</td>'+
						'</tr>'+
						'<tr align="center">'+
							'<td>Dirección: </td><td>'+dato.direccion+'</td>'+
               			'</tr>';
							
			if(dato.tel_fijo != 0)
			{
				codigoHTML+='<tr align="center">'+
								'<td>Telefono: </td><td>'+dato.tel_fijo+'</td>'+
							'</tr>';
			}
			codigoHTML+='<tr align="center">'+
							'<td>Celular: </td><td>'+dato.celular+'</td>'+
						'</tr>';
			$('#tabla_perfil').html(codigoHTML);
		});
	}).fail(function()
	{
		var db = openDatabase('Smartempdb', '1.0', 'Test DB', 1000000);
		db.transaction(function (tx) {
		  tx.executeSql('SELECT * FROM USER', [], function (tx, results) {
		   var len = results.rows.length, i;
		   for (i = 0; i < len; i++){
			   //navigator.notification.alert('mercadoActual'+results.rows.item(i).id);
			var codigoHTML= '<tr align="center">'+
							'<td colspan="2">'+results.rows.item(i).name+'</td>'+
						'</tr>'+
						'<tr align="center">'+
							'<td>Dirección: </td><td>'+results.rows.item(i).direccion+'</td>'+
               			'</tr>';
			codigoHTML+='<tr align="center">'+
							'<td>Celular: </td><td>'+results.rows.item(i).celular+'</td>'+
						'</tr>';
			$('#tabla_perfil').html(codigoHTML);
		   }
		 }, null);
		});
	});
	var codigoPer= getUrlVars()['codigo_usuario'];
	//navigator.notification.alert(codigoPer);
	document.getElementById('perfilUserPer').setAttribute('href','perfil.html?codigo_usuario='+codigoPer);
	document.getElementById('comprasUserPer').setAttribute('href','compras.html?codigo_usuario='+codigoPer);
	document.getElementById('mercadosUserPer').setAttribute('href','list_mercado.html?codigo_usuario='+codigoPer);
	document.getElementById('salirSesionPer').setAttribute('onClick','deleteSesion('+codigoPer+');');
	$.getJSON(serviceURL + 'smartphuy_datosUsuario.php?cedula='+codigoPer).done(function(data)
	{
		var datosU = data.items;
		$.each(datosU, function(index, datoU) 
			{
				//navigator.notification.alert(datoU.nombre+' '+datoU.apellido);
				$('#User_ActivoPer').html(datoU.nombre+' '+datoU.apellido);
			});
	});
}
function getUrlVars() 
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}