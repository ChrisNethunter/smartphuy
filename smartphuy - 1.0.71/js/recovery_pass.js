/*Aplicacion de la empresa COMERXION SAS. Todos los derechos reservados a COMERXION SAS.
	Diseño y Programación - Jorge Ivan Carrillo.*/
$('#PageRecovery').live('pageshow', function(event) 
{	
	$('#form_recovery_pass').submit(recovery_password);
});
function recovery_password(evento)
{
	evento.preventDefault();
	var correo = $('#correo').val();
	$.getJSON(serviceURL + 'smartphuy_recoverypass.php?correo='+correo);
	navigator.notification.alert('Verifica el Buzón de Mensajes de '+correo, null, 'Information');
}