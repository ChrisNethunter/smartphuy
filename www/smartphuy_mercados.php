<?php
include 'smartphuy_config1.php';

$sql = "SELECT * FROM mercados";

try 
{
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->query('SET CHARACTER SET utf8');
	$stmt = $dbh->query($sql);  
	$mercados= $stmt->fetchAll(PDO::FETCH_OBJ);
	$dbh = null;
	echo '{"items":'. json_encode($mercados) .'}'; 
} 
catch(PDOException $e) 
{
	echo '{"error":{"text":'. $e->getMessage() .'}}'; 
}


?>